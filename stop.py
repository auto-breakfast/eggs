from pycirculate.anova import AnovaController
import datetime


# Set the temp to 130.0F.
TEMP = 130.0

# Can be found with `sudo hcitool lescan`
ANOVA_MAC_ADDRESS = "78:A5:04:3E:42:F3"

def main():
    ctrl = AnovaController(ANOVA_MAC_ADDRESS)
    print datetime.datetime.now()
    print ctrl.read_timer(), ctrl.read_unit()
    print ctrl.stop_anova()


if __name__ == "__main__":
    main()

