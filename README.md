## GitLab AutoBreakfast

### Prerequisites
* `sudo apt-get install libglib2.0-dev`
* `pip install bluepy`

##

References / Thanks
* [Erik Wicktrom](http://www.erikwickstrom.com/)'s project [pycirculate](https://github.com/erikcw/pycirculate)