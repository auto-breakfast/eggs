from pycirculate.anova import AnovaController
import datetime

# Recipe https://recipes.anovaculinary.com/recipe/sous-vide-egg-bites-bacon-gruyere
# Set the temp to 172.0F.
TEMP = 172.0
TIME = 60

# Can be found with `sudo hcitool lescan`
ANOVA_MAC_ADDRESS = "78:A5:04:3E:42:F3"

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def main():
    ctrl = AnovaController(ANOVA_MAC_ADDRESS)
    print datetime.datetime.now()
    print "Current Temperature (F): " + ctrl.read_temp(), ctrl.read_unit()
    print "Current Timer: " + ctrl.read_timer(), ctrl.read_unit()
    print "Setting temperature to: " + bcolors.WARNING + ctrl.set_temp(TEMP) + bcolors.ENDC
    print "Setting timer to: " + bcolors.WARNING + ctrl.set_timer(TIME) + bcolors.ENDC
    print "Current Timer: " + ctrl.read_temp(), ctrl.read_unit()
    print ""
    print bcolors.HEADER + "GitLab AutoBreakfast" + bcolors.ENDC
    print ""
    print "           \033[38;5;88m`                        `"
    print "          :s:                      :s:"
    print "         `oso`                    `oso."
    print "         +sss+                    +sss+"
    print "        :sssss:                  -sssss:"
    print "       `ossssso`                `ossssso`"
    print "       +sssssss+                +sssssss+"
    print "      -ooooooooo-++++++++++++++-ooooooooo-"
    print "     \033[38;5;208m`:/\033[38;5;202m+++++++++\033[38;5;88mosssssssssssso\033[38;5;202m+++++++++\033[38;5;208m/:`"
    print "     -///\033[38;5;202m+++++++++\033[38;5;88mssssssssssss\033[38;5;202m+++++++++\033[38;5;208m///-"
    print "     .//////\033[38;5;202m+++++++\033[38;5;88mosssssssssso\033[38;5;202m+++++++\033[38;5;208m//////."
    print "     :///////\033[38;5;202m+++++++\033[38;5;88mosssssssso\033[38;5;202m+++++++\033[38;5;208m///////:"
    print "      .:///////\033[38;5;202m++++++\033[38;5;88mssssssss\033[38;5;202m++++++\033[38;5;208m///////:.`"
    print "        `-://///\033[38;5;202m+++++\033[38;5;88mosssssso\033[38;5;202m+++++\033[38;5;208m/////:-`"
    print "           `-:////\033[38;5;202m++++\033[38;5;88mosssso\033[38;5;202m++++\033[38;5;208m////:-`"
    print "              .-:///\033[38;5;202m++\033[38;5;88mosssso\033[38;5;202m++\033[38;5;208m///:-."
    print "                `.://\033[38;5;202m++\033[38;5;88mosso\033[38;5;202m++\033[38;5;208m//:.`"
    print "                   `-:/\033[38;5;202m+\033[38;5;88moo\033[38;5;202m+\033[38;5;208m/:-`"
    print "                      `-++-`\033[0m"
    print ""
    print bcolors.ENDC
    print ctrl.start_anova()
    print ctrl.start_timer()
    print ctrl.anova_status()


if __name__ == "__main__":
    main()

